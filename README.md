# AWS Auth Provider

This project builds a Docker image with an API able to retrieve authorization tokens from AWS APIs.

It is aimed at being used in GitLab CI as a [service container](https://docs.gitlab.com/ee/ci/services/)
in order to decouple the image of your jobs and the way AWS authorization tokens are retrieved.

## API usage

### The notion of `env_ctx`

This API supports the notion of `env_ctx`. It can either be guessed contextually (read next chapter), or explicitly passed in all API endpoints.

The `env_ctx` is used when retrieving a configuration value - say `SOME_SECRET`:

* the value will first be readed from `$AWS_{env_ctx}_SOME_SECRET`,
* if not valuated, will fallback to `$AWS_SOME_SECRET`.

This is therefore a way of specializing configuration variables to a specific context.

#### How is guessed `env_ctx`?

When not explicitly set, `env_ctx` is automatically guessed based on [GitLab predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html):

| `$CI_COMMIT_REF_NAME` | `env_ctx` value |
| --------------------- | --------------- |
| `master` or `main`    | **PROD** if `$CI_JOB_STAGE` is one of `publish`, `infra-prod`, `production`, `.post`<br/>**STAGING** otherwise |
| `develop`             | **INTEG** |
| _any other branch_    | **REVIEW** |

### Supported authentication methods

The API supports two authentication methods:

1. basic authentication with AWS access key ID & secret access key,
2. or [federated authentication using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/).

#### Basic authentication

If you wish to use this authentication method, you'll have pass the AWS access key ID & secret access key as environment variables.
The API is able to segregate variables per environment type.

The expected environment variables are `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` (with their specialized values depending on the `env_ctx`).

#### Federated authentication using OpenID Connect

The API supports [OpenID Connect to retrieve temporary credentials](https://docs.gitlab.com/ee/ci/cloud_services/aws/).

If you wish to use this authentication mode, please apply carefully the instructions from the GitLab guide, then provide the following variables to the API:

* `AWS_JWT` for the JWT token (using GitLab [ID Tokens](https://docs.gitlab.com/ee/ci/secrets/id_token_authentication.html)),
* `AWS_OIDC_ROLE_ARN`: the configured role ARN.

You may specialize those variables for the current `env_ctx`.

### `GET /ecr/auth/token`

This API retrieves an [ECR authorization token](https://docs.aws.amazon.com/AmazonECR/latest/APIReference/API_GetAuthorizationToken.html).

#### Query Parameters

| Name       | Description                                                   | Required              | 
|------------|---------------------------------------------------------------|-----------------------|
| `region`   | AWS region to use                                             | no _(can be retrieved from env)_ |
| `env_ctx`  | the [environment context to consider](#the-notion-of-env_ctx) | no _(can be guessed)_ |

### `GET /ecr/auth/username`

This API retrieves the [ECR authorization token](https://docs.aws.amazon.com/AmazonECR/latest/APIReference/API_GetAuthorizationToken.html) username
(can be used in `docker login` command).

#### Query Parameters

| Name       | Description                                                  | Required              | 
|------------|---------------------------------------------------------------|-----------------------|
| `region`   | AWS region to use                                             | no _(can be retrieved from env)_ |
| `env_ctx`  | the [environment context to consider](#the-notion-of-env_ctx) | no _(can be guessed)_ |

### `GET /ecr/auth/password`

This API retrieves the [ECR authorization token](https://docs.aws.amazon.com/AmazonECR/latest/APIReference/API_GetAuthorizationToken.html) password
(can be used in `docker login` command).

#### Query Parameters

| Name       | Description                                                   | Required              | 
|------------|---------------------------------------------------------------|-----------------------|
| `region`   | AWS region to use                                             | no _(can be retrieved from env)_ |
| `env_ctx`  | the [environment context to consider](#the-notion-of-env_ctx) | no _(can be guessed)_ |

### `GET /codeartifact/auth/token`

This API retrieves an [CodeArtifact authorization token](https://docs.aws.amazon.com/codeartifact/latest/APIReference/API_GetAuthorizationToken.html).
(can be used with [pip](https://docs.aws.amazon.com/codeartifact/latest/ug/python-configure-pip.html))


#### Query Parameters

| Name       | Description                                                   | Required              |
|------------|---------------------------------------------------------------|-----------------------|
| `domain`   | CodeArtifact domain to use                                    | no _(can be retrieved from env)_ |
| `domain_owner` | CodeArtifact domain owner to use                           | no _(can be retrieved from env)_ |
| `region`   | AWS region to use                                             | no _(can be retrieved from env)_ |
| `env_ctx`  | the [environment context to consider](#the-notion-of-env_ctx) | no _(can be guessed)_ |

### `GET /codeartifact/repository/endpoint`

This API retrieves the [CodeArtifact repository endpoint](https://docs.aws.amazon.com/codeartifact/latest/APIReference/API_GetRepositoryEndpoint.html).

#### Query Parameters

| Name       | Description                                                   | Required              |
|------------|---------------------------------------------------------------|-----------------------|
| `format`   | the format of the endpoint (e.g. `npm`, `pypi`, `maven`) [full list](https://docs.aws.amazon.com/codeartifact/latest/APIReference/API_GetRepositoryEndpoint.html#CodeArtifact-GetRepositoryEndpoint-request-format) | yes |     
| `domain`   | CodeArtifact domain to use                                    | no _(can be retrieved from env)_ |
| `domain_owner` | CodeArtifact domain owner to use                           | no _(can be retrieved from env)_ |
| `repository` | CodeArtifact repository to use                              | no _(can be retrieved from env)_ |
| `region`   | AWS region to use                                             | no _(can be retrieved from env)_ |
| `env_ctx`  | the [environment context to consider](#the-notion-of-env_ctx) | no _(can be guessed)_ |
  



## Use in GitLab CI

Finally, the Docker image can be used in your GitLab CI files as follows:

```yaml
variables:
  # required by the AWS auth provider service
  AWS_REGION: us-east-1
  # default OIDC role ARN (OIDC authentication method)
  AWS_OIDC_ROLE_ARN: "arn:aws:iam::123456789012:role/cicd-role-default"
  # specialized AWS region for 'TEST1'
  AWS_TEST1_REGION: eu-west-1
  # specialized OIDC role ARN for 'TEST1'
  AWS_TEST1_OIDC_ROLE_ARN: "arn:aws:iam::123456789012:role/cicd-role-test1"

docker-build-step1:
  image: docker:24.0.5
  services:
    # add Docker daemon
    - name: docker:24.0.5-dind
    # add AWS Auth Provider as a service
    - name: $CI_REGISTRY/to-be-continuous/tools/aws-auth-provider:latest
      alias: aws-auth-provider
  variables:
    # my variable (not required by AWS auth provider service)
    ecr_registry: 123456789012.dkr.ecr.$AWS_TEST1_REGION.amazonaws.com
  id_tokens:
    # required by the AWS auth provider service (OIDC authentication method)
    AWS_JWT:
      aud: "https://123456789012.dkr.ecr.$AWS_TEST1_REGION.amazonaws.com"
  before-script:
    # retrieve authorization from ECR (in context 'TEST1')
    - ecr_auth_username=$(curl -s -S -f "http://aws-auth-provider/ecr/auth/username?env_ctx=TEST1")
    - ecr_auth_password=$(curl -s -S -f "http://aws-auth-provider/ecr/auth/password?env_ctx=TEST1")
    # login
    - echo "$ecr_token" | docker login --username "$ecr_auth_username" --password-stdin "$ecr_auth_password"
    - docker info
  script:
    # build and push my image
    - docker build --tag ecr_registry/my-image:latest .
    - docker push ecr_registry/my-image:latest
```
